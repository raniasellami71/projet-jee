package models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Entity;

@Entity
@Table(name="Autorisation", 
	   uniqueConstraints={@UniqueConstraint(columnNames={"ID"})})
public class Autorisation {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", nullable=false, unique=true, length=11)
	private int id;
	
	@Column(name="employe_id", length=20, nullable=false)
	private String employe_id;
	
	@Column(name="date_debut", length=20, nullable=false)
	private Date date_debut;
	
	@Column(name="date_fin", length=20, nullable=false)
	private Date date_fin;
	
	@Column(name="nbHeures", length=20, nullable=false)
	private String nbHeures;
	
	public Autorisation(int id, String employe_id, Date date_debut, Date date_fin, String nbHeures) {
		super();
		this.id = id;
		this.employe_id = employe_id;
		this.date_debut = date_debut;
		this.date_fin = date_fin;
		this.nbHeures = nbHeures;
	}
	public Autorisation(){}
	public Autorisation(String employe_id, Date date_debut, Date date_fin, String nbHeures) {
	
		this.employe_id = employe_id;
		this.date_debut = date_debut;
		this.date_fin = date_fin;
		this.nbHeures = nbHeures;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmploye_id() {
		return employe_id;
	}

	public void setEmploye_id(String employe_id) {
		this.employe_id = employe_id;
	}

	public Date getDate_debut() {
		return date_debut;
	}

	public void setDate_debut(Date date_debut) {
		this.date_debut = date_debut;
	}

	public Date getDate_fin() {
		return date_fin;
	}

	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}

	public String getnbHeures() {
		return nbHeures;
	}

	public void setnbHeures(String nbHeures) {
		this.nbHeures = nbHeures;
	}
	
	
}

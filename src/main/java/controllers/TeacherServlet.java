package controllers;

import models.Teacher;

import java.io.IOException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;

import dao.HibernateUtil;

/**
 * Servlet implementation class TeacherServlet
 */

public class TeacherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TeacherServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletContext application = getServletContext();
		RequestDispatcher rd = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria crit = session.createCriteria(Teacher.class);
		int id = 0;
		if(request.getParameter("id")!=null)
			id = Integer.parseInt(request.getParameter("id").replaceAll("[^\\.0123456789]",""));
		if(id != 0 ) {
			Teacher currentTeacher = (Teacher) session.get(Teacher.class, id);
			request.setAttribute("currentTeacher", currentTeacher);
			rd = application.getRequestDispatcher("/EditTeacher.jsp");
			rd.forward(request, response);
		}else
		{
		
		List<Teacher> lstTeachers = crit.list();
		lstTeachers.forEach(x -> System.out.println(x.toString()));
		
		request.setAttribute("lstTeachers", lstTeachers);
	
		rd = application.getRequestDispatcher("/IndexTeacher.jsp");
		rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Date birthday = null;
			DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
			try {
				birthday = (Date) df.parse(URLDecoder.decode(request.getParameter("birthday"), "UTF-8"));
            } catch (ParseException e) {
                e.printStackTrace();
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                response.flushBuffer();
            }
			String first_name = request.getParameter("first_name");
			String last_name = request.getParameter("last_name");
			String email = request.getParameter("email");
			String institution = request.getParameter("institution");
			int nb_heures_disponibles = 208;
			String phone = request.getParameter("phone");
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			
			if(request.getParameter("id")!=null) {
				int id = Integer.parseInt(request.getParameter("id"));
				Teacher teacher = new Teacher(id, first_name, last_name, birthday,nb_heures_disponibles, email, institution, phone);
				session.update(teacher);
				tx.commit();
				response.sendRedirect("/jeeProject2/TeacherServlet");
			}else {
				Teacher teacher = new Teacher(first_name, last_name, birthday,nb_heures_disponibles, email, institution, phone);
				session.save(teacher);
				tx.commit();
				response.sendRedirect("/jeeProject2/TeacherServlet");
			}
			
		
			
		}catch (Exception e) {
			System.err.println("Exception <DoPost>(TeacherServlet) "+e.getMessage());
		}
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println(request.getParameter("id"));
		
		if(request.getParameter("id")!=null) {
			  
			int id = Integer.parseInt(request.getParameter("id"));
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			Teacher teacher = (Teacher) session.get(Teacher.class, id);
			session.delete("id",teacher);
			tx.commit();
		}
	}

}

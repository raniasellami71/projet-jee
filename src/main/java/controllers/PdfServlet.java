package controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.Autorisation;
import models.Teacher;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.http.HttpSession;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import dao.HibernateUtil;

import java.io.FileOutputStream;
import org.dom4j.DocumentException;

import org.hibernate.Session;

/**
 * Servlet implementation class PdfServlet
 */
public class PdfServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PdfServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     
			Session sessionHibernate = HibernateUtil.getSessionFactory().openSession();
            HttpSession session = (HttpSession) request.getSession();
            int id = Integer.parseInt(request.getParameter("id").replaceAll("[^\\.0123456789]",""));
            Autorisation currentAuto = (Autorisation) sessionHibernate.get(Autorisation.class, id);
            Teacher teacher = (Teacher) sessionHibernate.get(Teacher.class,Integer.parseInt( currentAuto.getEmploye_id()));
            session.setAttribute("auto", currentAuto);
            session.setAttribute("teacher", teacher);
            System.out.println("Impression PDF created successfully!");
     
	}

}

package controllers;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.Autorisation;
import models.Teacher;

import java.io.IOException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;

import dao.HibernateUtil;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.WeekFields;
import java.util.Locale;

/**
 * Servlet implementation class AutorisationServlet
 */
public class AutorisationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AutorisationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletContext application = getServletContext();
		RequestDispatcher rd = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria crit = session.createCriteria(Autorisation.class);
		Criteria critTeachers = session.createCriteria(Teacher.class);
		int id = 0;
		if(request.getParameter("id")!=null)
			id = Integer.parseInt(request.getParameter("id").replaceAll("[^\\.0123456789]",""));
		if(id != 0 || request.getParameter("Go") != null)  {
			List<Teacher> lstTeachers = critTeachers.list();
			lstTeachers.forEach(x -> System.out.println(x.toString()));
			request.setAttribute("lstTeachers", lstTeachers);
			Autorisation currentAuto = (Autorisation) session.get(Autorisation.class, id);
			request.setAttribute("currentAuto", currentAuto);
			rd = application.getRequestDispatcher("/EditAutorisation.jsp");
			rd.forward(request, response);
		}else
		{
		List<Autorisation> lstAutorisations = crit.list();
		lstAutorisations.forEach(x -> System.out.println(x.toString()));
		request.setAttribute("lstAuto", lstAutorisations);
		rd = application.getRequestDispatcher("/IndexAutorisation.jsp");
		rd.forward(request, response);
		
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Date date_debut = null;
			Date date_fin = null;
			DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
			try {
				date_debut = (Date) df.parse(URLDecoder.decode(request.getParameter("date_debut"), "UTF-8"));
				date_debut = (Date) df.parse(URLDecoder.decode(request.getParameter("date_fin"), "UTF-8"));
            } catch (ParseException e) {
                e.printStackTrace();
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                response.flushBuffer();
            }
			String employe_id = request.getParameter("employe_id");
			Duration duration = Duration.between(date_debut.toInstant(), date_debut.toInstant());
			String nbHeures =  request.getParameter("nbHeures");
			
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			
			int current_year_remaining_hours = this.getNumberOfWeeksFromToday() * 4;
			Teacher teacher = (Teacher) session.get(Teacher.class,Integer.parseInt(request.getParameter("employe_id")));
			
			int  input_nbHeures = Integer.parseInt(nbHeures) ;
			
			int expected_teacher_nb_heures = teacher.getNbHeures_consome() + input_nbHeures;
			
			if(request.getParameter("id")!=null) {
				int id = Integer.parseInt(request.getParameter("id"));
				Autorisation autorisation = new Autorisation(id, employe_id, date_debut, date_debut, nbHeures.toString());
				session.update(autorisation);
				tx.commit();
				response.sendRedirect("/jeeProject2/AutorisationServlet");
			}else {
				Autorisation autorisation = new Autorisation(employe_id, date_debut, date_debut, nbHeures.toString());
				session.save(autorisation);
				tx.commit();
				response.sendRedirect("/jeeProject2/AutorisationServlet");
			}
			
		}catch (Exception e) {
			System.err.println("Exception <DoPost>(TeacherServlet) "+e.getMessage());
		}
	}
	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println(request.getParameter("id"));
		
		if(request.getParameter("id")!=null) {
			  
			
			int id = Integer.parseInt(request.getParameter("id"));
			
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			Autorisation autorisation = (Autorisation) session.get(Autorisation.class, id);
			session.delete("id",autorisation);
			tx.commit();
		}
	}
	
	 public static int getNumberOfWeeksFromToday() {
	        // Get today's date
	        LocalDate today = LocalDate.now();

	        // Get the current year
	        int currentYear = today.getYear();

	        // Create a WeekFields object to determine the first week of the year
	        WeekFields weekFields = WeekFields.of(Locale.getDefault());

	        // Get the last day of the current year
	        LocalDate lastDayOfYear = LocalDate.of(currentYear, 12, 31);

	        // Calculate the number of weeks from today until the last day of the year
	        long numberOfWeeks = today.until(lastDayOfYear, ChronoUnit.WEEKS);

	        // Adjust for the first week of the year if today is in the first week
	        if (today.get(weekFields.weekOfWeekBasedYear()) == 1) {
	            numberOfWeeks++;
	        }

	        return (int) numberOfWeeks;
	    }


}

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<style>
body {
  margin: 0;
}

ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  width: 15%;
  background-color: #f1f1f1;
  position: fixed;
  height: 100%;
  overflow: auto;
}

li a {
  display: block;
  color: #000;
  padding: 8px 16px;
  text-decoration: none;
}

li a.active {
  background-color: #7EC8E3

;
  color: white;
}

li a:hover:not(.active) {
  background-color: #7EC8E3;
  color: white;
}
img {
  border-radius: 50%;
  margin-left:20px;
  margin-top:20px;
  margin-bottom:20px;
  width: 80%;
  height: 80%;
}
</style>
<head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>

</head>
<body>
<ul>
  <span style="padding-left:8px; padding-top:8px; color:#7EC8E3; font-size:30px">Welcome <%=  request.getSession().getAttribute("username") %></span>
  <li><img src="https://img.freepik.com/free-vector/illustration-businessman_53876-5856.jpg" alt="Avatar" class="avatar"></li>
  <li><a href="TeacherServlet">Enseignants</a></li>
  <li><a href="AutorisationServlet">Autorisations</a></li>
  <li><a href="login.jsp">Deconnexion</a></li>
</ul>
</body>
</html>
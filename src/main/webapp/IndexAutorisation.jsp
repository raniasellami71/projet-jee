<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<!DOCTYPE html>

<html>

		<meta charset="ISO-8859-1"><script src="https://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>
		<title>Enseignants</title>

<script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
<style>
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  padding: 8px;
  text-align: left;
  border-bottom: 1px solid #ddd;
}
.button {
  /* Green */
  border:none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  margin: 4px 2px;
  cursor: pointer;
}
.button1 {font-size: 8px;background-color: red; }
.button2 {font-size: 8px;background-color: green; }
.button3 {font-size: 8px;  background-color: #f1f1f1; }
.button4 {font-size: 8px;  background-color: #7EC8E3; }
</style>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="/Navbar.jsp"/>
<div style="margin-left:15%;padding:1px 16px;padding-top:50px;height:1000px;">
<button class="button button3" ><a href="AutorisationServlet?Go=AddAuto"><i class="fas fa-plus" style="font-size:20px;color:#7EC8E3};"></i></a></button>
<table style="boloder=1">
		<thead> 
			<tr>
				<th>Employe </th>
				<th>date debut</th>
				<th>date fin</th>
				<th>nb Heures</th> 
			    <th>Actions </th>
			</tr>
		</thead>
		<tbody>


<%
java.util.List<models.Autorisation> autorisations = (java.util.List<models.Autorisation>) request.getAttribute("lstAuto");
for (models.Autorisation autorisation : autorisations) {
%>
 	<tr>
		
		<td><%= autorisation.getEmploye_id() %></td>
		<td><%= autorisation.getDate_debut() %></td> 
		<td><%= autorisation.getDate_fin() %></td>
	     <td><%= autorisation.getnbHeures() %></td>
		<td>
			<button class="button button1" onclick="deleteAuto(<%= autorisation.getId() %>)" ><i class="fas fa-trash" style="font-size:20px;color:white;"></i></button>
			<button class="button button2" ><a href="AutorisationServlet?id=<%= autorisation.getId() %>"><i class="fas fa-edit" style="font-size:20px;color:white;"></i></a></button>
			<button class="button button4" id="printButton" onclick="impressionAuto(<%= autorisation.getId() %>)"><i class="fas fa-print" style="font-size:20px;color:white;"></i></button>
			
		</td>
	</tr> 
<%
}
%>
		</tbody>
	</table>
	</div>
	<script>
function deleteAuto(id){
    
    $.ajax({
            method: "DELETE",
            url: "/jeeProject2/AutorisationServlet?id="+id,
            success: function(data) {
            	window.location.href="/jeeProject2/AutorisationServlet";
            },
            error:  function(xhr, str){
                    alert("Erreur lors de suppression")
            }
            }).done(function() {
        });
}

function impressionAuto(id){
	
	  $.ajax({
          method: "POST",
          url: "/jeeProject2/PdfServlet?id="+id,
          success: function(data) {
        		let objFra = document.createElement('iframe');     // Create an IFrame.
                objFra.style.visibility = 'hidden';                // Hide the frame.
                objFra.src = 'pdf.jsp';  // Set source.
                document.body.appendChild(objFra);  // Add the frame to the web page.
                objFra.contentWindow.focus();       // Set focus.
                objFra.contentWindow.print();   
          },
          error:  function(xhr, str){
                  alert("Erreur lors de suppression")
          }
          }).done(function() {
      });
        // Print it.
    
}
</script>
</body>

</html>
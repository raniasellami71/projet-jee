
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1"><script src="https://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>
		<title>Edition enseignant</title>
	</head>
<style>
.button {
  width:100%;
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  padding-top:15px;
}
* {
  box-sizing: border-box;
}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}

label {
  padding: 12px 12px 12px 0;
  display: inline-block;
}

input[type=submit] {
  background-color: #04AA6D;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  float: right;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

.col-25 {
  float: left;
  width: 25%;
  margin-top: 6px;
}

.col-75 {
  float: left;
  width: 75%;
  margin-top: 6px;
}

/* Clear floats after the columns */
.row::after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .col-25, .col-75, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}
</style>
<body>
<jsp:include page="/Navbar.jsp"/>
<div style="margin-left:15%;padding:1px 16px;padding-top:50px;height:1000px;">
<%if ( request.getAttribute("currentTeacher") == null ) {%>
<h1>Ajouter Enseignant</h1>
<%} else {%>
<h1>Modifier Enseignant</h1>
<%} %>
<div class="container">

	<form action="TeacherServlet" method="POST">
	
	<%if ( request.getAttribute("currentTeacher") != null ) {%>
		<input type="hidden" name="id" id="id"  value="${currentTeacher.id}">
	<%} %>
  <div class="row">
    <div class="col-25">
      <label for="fname">Nom</label>
    </div>
    <div class="col-75">
      <input type="text" name="first_name" id="first_name"  value="${currentTeacher.first_name}">
    </div>
  </div>
  
    <div class="row">
    <div class="col-25">
      <label for="fname">Prenom</label>
    </div>
    <div class="col-75">
      <input type="text" name="last_name" id="last_name"  value="${currentTeacher.last_name}">
    </div>
  </div>
  
     <div class="row">
    <div class="col-25">
      <label for="fname">Date de naissance</label>
    </div>
    <div class="col-75">
   <input type="date" name="birthday" id="birthday" 
	value="fmt:formatDate value="${currentTeacher.birthday}" pattern="yyyy-MM-dd">
    </div>
  </div>
	   <div class="row">
    <div class="col-25">
      <label for="fname">Email</label>
    </div>
    <div class="col-75">
  <input type="text" name="email" id="email" value="${currentTeacher.email}">	
  </div>
  </div>
  
<div class="row">
   <div class="col-25">
   <label for="fname">Institution</label>
   </div>
   <div class="col-75">
  <input type="text" name="institution" id="institution" value="${currentTeacher.institution}">	
  </div>
  </div> 
  
  
 	<div class="row">
    <div class="col-25">
     <label for="fname">Telephone</label>
    </div>
    <div class="col-75">
  <input type="text" name="phone" id="phone" value="${currentTeacher.phone}">	
  </div>
  </div>
		
<div class="row">
	<button  id="myModal" class="btn btn-success"  type="submit" style="width:fit-content;margin:10px;" >Enregistrer</button></div></form>
</div>

</body>
</html>
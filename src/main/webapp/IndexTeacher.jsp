<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<!DOCTYPE html>

<html>

		<meta charset="ISO-8859-1"><script src="https://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>
		<title>Enseignants</title>

<script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
<style>
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  padding: 8px;
  text-align: left;
  border-bottom: 1px solid #ddd;
}
.button {
  /* Green */
  border:none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  margin: 4px 2px;
  cursor: pointer;
}
.button1 {font-size: 8px;background-color: red; }
.button2 {font-size: 8px;background-color: green; }
.button3 {font-size: 8px;  background-color: #f1f1f1; }
</style>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="/Navbar.jsp"/>
<div style="margin-left:15%;padding:1px 16px;padding-top:50px;height:1000px;">
<button class="button button3" onclick="/EditTeacher.jsp" ><a href="EditTeacher.jsp"><i class="fas fa-plus" style="font-size:20px;color:#7EC8E3};"></i></a></button>
<table style="boloder=1">
		<thead> 
			<tr>
				<th>Nom</th>
				<th>Pr�nom</th>
				<th>Date de naissance</th>
				<th>Email</th>
				<th>Institution</th>
				<th>Num�ro de telephone</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>


<%
java.util.List<models.Teacher> teachers = (java.util.List<models.Teacher>) request.getAttribute("lstTeachers");
for (models.Teacher teacher : teachers) {
%>
 	<tr>
		<td><%= teacher.getFirst_name() %></td>
		<td><%= teacher.getLast_name() %></td>
		<td><%= teacher.getBirthday() %></td>
		<td><%= teacher.getEmail() %></td>
		<td><%= teacher.getInstitution() %></td>
		<td><%= teacher.getPhone() %></td>
		<td>
			<button class="button button1" onclick="deleteTeacher(<%= teacher.getId() %>)" ><i class="fas fa-trash" style="font-size:20px;color:white;"></i></button>
			<button class="button button2" ><a href="TeacherServlet?id=<%= teacher.getId() %>"><i class="fas fa-edit" style="font-size:20px;color:white;"></i></a></button>
			
		</td>
	</tr> 
<%
}
%>
		</tbody>
	</table>
	</div>
	<script>
function deleteTeacher(id){
    
    $.ajax({
            method: "DELETE",
            url: "/jeeProject2/TeacherServlet?id="+id,
            success: function(data) {
            	window.location.href="/jeeProject2/TeacherServlet";
            },
            error:  function(xhr, str){
                    alert("Erreur lors de suppression")
            }
            }).done(function() {
        });
}
</script>
</body>

</html>
<!DOCTYPE html>
<%@page import="models.Teacher"%>
<%@page import="models.Autorisation"%>
<html>
<head>
<style>
table {
overflow: scroll; /* Scrollbar are always visible */
overflow: auto;   /* Scrollbar is displayed as it's needed */
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {background-color: #f2f2f2;}
</style>
</head>
<body>
<%
 Autorisation auto = (Autorisation) request.getSession().getAttribute("auto");
 Teacher teacher = (Teacher) request.getSession().getAttribute("teacher");
%>



<div style="text-align:center">
<h1>Autorisation de lenseignant <strong><%= teacher.getFirst_name()+" "+teacher.getLast_name() %></strong></h2>
</div>

<br>

<div>
	<strong>Nom employee : </strong> <span><%= teacher.getFirst_name()+" "+teacher.getLast_name() %></span>
</div>

<div>
	<strong>Date de d�ebut : </strong> <span><%= auto.getDate_debut() %></span>
</div>

<div>
	<strong>Date de fin : </strong> <span><%= auto.getDate_fin() %></span>
</div>

<div>
	<strong>Nombre d'heures : </strong> <span><%= auto.getnbHeures() %></span>
</div>

</body>
</html>



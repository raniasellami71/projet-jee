
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1"><script src="https://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>
		<title>Edition enseignant</title>
	</head>
<style>
.button {
  width:100%;
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  padding-top:15px;
}
* {
  box-sizing: border-box;
}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}

label {
  padding: 12px 12px 12px 0;
  display: inline-block;
}

input[type=submit] {
  background-color: #04AA6D;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  float: right;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

.col-25 {
  float: left;
  width: 25%;
  margin-top: 6px;
}

.col-75 {
  float: left;
  width: 75%;
  margin-top: 6px;
}

/* Clear floats after the columns */
.row::after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .col-25, .col-75, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}
</style>
<body>
<jsp:include page="/Navbar.jsp"/>
<div style="margin-left:15%;padding:1px 16px;padding-top:50px;height:1000px;">
<%if ( request.getAttribute("currentAuto") == null ) {%>
<h1>Ajouter Autorisation</h1>
<%} else {%>
<h1>Modifier Autorisation</h1>
<%} %>

<%if(request.getAttribute("erreur") != null) {%>

<div class="alert alert-danger" role="alert">
  <%= request.getAttribute("erreur") %>
</div>
<%} %>

<div class="container">

	<form action="AutorisationServlet" method="POST">
	
	<%if ( request.getAttribute("currentAuto") != null ) {%>
		<input type="hidden" name="id" id="id"  value="${currentAuto.id}">
	<%} %>

  
    <div class="row">
    <div class="col-25">
      <label for="fname">Employe</label>
    </div>
    <div class="col-75">
    <select name="employe_id" id="employe_id">
    
<%
java.util.List<models.Teacher> teachers = (java.util.List<models.Teacher>) request.getAttribute("lstTeachers");
for (models.Teacher teacher : teachers) {
%>
    <option value="<%= teacher.getId()%>"><%= teacher.getFirst_name()+" "+teacher.getLast_name()%></option>
    
    <%} %>
    </select>
    </div>
    </div>
  
    <div class="row">
    <div class="col-25">
    <label for="fname">Date de debut</label>
    </div>
    <div class="col-75">
   <input type="date" name="date_debut" id="date_debut" 
	value="fmt:formatDate value="${currentAuto.date_debut}" pattern="yyyy-MM-dd">
   </div>
   </div>
  
   <div class="row">
    <div class="col-25">
      <label for="fname">Date de fin</label>
    </div>
    <div class="col-75">
   <input type="date" name="date_fin" id="date_fin" 
	value="fmt:formatDate value="${currentAuto.date_fin}" pattern="yyyy-MM-dd">
    </div>
  </div>
  
   <div class="row">
    <div class="col-25">
      <label for="fname">nb Heures</label>
    </div>
    <div class="col-75">
   <input type="text" name="nbHeures" id="nbHeures" 
	value="${currentAuto.nbHeures}" >
    </div>
  </div>

  
<div class="row">
	<button  id="myModal" class="btn btn-success"  type="submit" style="width:fit-content;margin:10px;" >Enregistrer</button></div></form>
</div>
</div>

</body>
</html>